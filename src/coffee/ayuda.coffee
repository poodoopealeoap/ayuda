class Ayuda
    constructor: (options) ->
        @options =
            autoHeadings: options.autoHeadings ? defaults.autoHeadings
            animationSpeed: options.animationSpeed ? defaults.animationSpeed
            bootstrap: options.bootstrap ? defaults.bootstrap

        @content = $ ".ayuda-content"
        buildElements.call @
        setStyles.call @
        setListeners.call @
