buildNav = ->
    "<nav class='ayuda-nav'>
        <ul>
            #{
                (for link in @content.children "[data-ayuda-link]"
                    "<li data-ayuda-link='#{$(link).attr "data-ayuda-link"}'>#{$(link).attr "data-ayuda-link"}
                        #{
                            sub = $(link).children "[data-ayuda-link]"
                            if sub.length > 0
                                "<ul>
                                    #{("<li data-ayuda-link='#{$(link2).attr "data-ayuda-link"}'>#{$(link2).attr "data-ayuda-link"}</li>" for link2 in sub).join ""}
                                </ul>"
                            else
                                ""
                        }
                    </li>").join ""
            }
        </ul>
    </nav>"