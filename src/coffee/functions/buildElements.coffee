buildElements = ->
    $ "body"
        .append "<div class='#{"col-lg-2 col-sm-3" if @options.bootstrap} ayuda-nav-wrapper'>#{buildNav.call @}</div><div class='#{"col-lg-10 col-sm-9 col-xs-12 col-lg-offset-2 col-sm-offset-3" if @options.bootstrap} ayuda-content-wrapper'></div>"

    @nav = $ ".ayuda-nav"

    $ ".ayuda-content-wrapper"
        .append @content
    
    if @options.autoHeadings
        @content.children('[data-ayuda-link]').each ->
            $(@).prepend "<h1>#{$(@).attr "data-ayuda-link"}</h1>"
            $(@).children('[data-ayuda-link]').each ->
                $(@).prepend "<h3>#{$(@).attr "data-ayuda-link"}</h3>"
