setListeners = ->
    nav = @nav
    content = @content
    options = @options
    sections = {}
    lookahead = window.innerHeight * 0.1

    updateSectionRanges = ->
        $("[data-ayuda-link]").each ->
            if @tagName isnt "LI"
                sections[$(@).attr "data-ayuda-link"] =
                    start: $(@).offset().top
                    end: parseInt($(@).css("height")) + $(@).offset().top
    
    # setNavVisibility = ->
    #     if @innerWidth < 768
    #         nav.parent().css "margin-left", "-200px"
    #     else
    #         nav.parent().css "margin-left", "0"

    setScrollPastEnd = ->
        $ "section[data-ayuda-link], div[data-ayuda-link]"
            .last().css
                "min-height": window.innerHeight

    $ window
        .on "load resize hashchange", ->
            updateSectionRanges()
            #setNavVisibility.call @
            setScrollPastEnd.call @
            hash = window.location.hash
            if hash.length > 0
                hash = hash.split('#')[1]
                if hash.indexOf('/') isnt -1
                    hash = hash.split('/')
                    link = "li[data-ayuda-link='#{hash[0]}']";
                    for part, i in hash
                        if i > 0
                            link += " li[data-ayuda-link='#{part}']"
                    $(link).click()
                else
                    $("li[data-ayuda-link='#{hash}']").click()
            
    navSpy = ->
        scrollY = @scrollY
        if kula.browser() is "IE"
            scrollY = document.documentElement.scrollTop
        for section of sections
            if scrollY >= (sections[section].start - lookahead) and scrollY <= (sections[section].end - lookahead)
                $("li[data-ayuda-link='#{section}']").addClass("active").children("ul").slideDown options.animationSpeed
            else
                $("li[data-ayuda-link='#{section}']").removeClass("active").children("ul").slideUp options.animationSpeed
    
    $ window
        .on "load scroll", navSpy

    scrollToSection = ->
        section = $(@).attr("data-ayuda-link") ? $(@).attr("data-ayuda-anchor")
        $(window).off "scroll", navSpy
        $('html,body').animate
            scrollTop: sections[section].start - (lookahead / 5)
        , options.animationSpeed * 2
        setTimeout ->
            $(window).on "scroll", navSpy
            $(window).scroll()
        , options.animationSpeed * 2

    @nav.children("ul").children("li").click (e) ->
        $(@).siblings("li").removeClass("active").children('ul').slideUp options.animationSpeed
        $(@).addClass "active"
        $(@).children("ul").slideDown options.animationSpeed
        
        clickedChild = false
        for child in $(@).children("ul").children("li")
            if e.target is child
                clickedChild = true
        if !clickedChild
            scrollToSection.call @

    $("a[data-ayuda-anchor]").click (e) ->
        e.preventDefault()
        scrollToSection.call @
    
    @nav.children("ul").children("li").children("ul").children("li").click scrollToSection

    

    if kula.lightLevel($("body").css("background-color")) < 30
        document.styleSheets[document.styleSheets.length - 1].insertRule(".ayuda-nav > ul > li > ul > li:hover,.ayuda-nav > ul > li:hover {background-color:#{kula.lighten($("body").css("background-color"), if kula.lightLevel($("body").css("background-color")) < 2.5 then 500 else 30)};}", document.styleSheets[document.styleSheets.length - 1].cssRules.length)
        document.styleSheets[document.styleSheets.length - 1].insertRule(".ayuda-nav > ul > li > ul > li.active,.ayuda-nav > ul > li.active {border-color: #{kula.lighten("#2780e3", 20)}}", document.styleSheets[document.styleSheets.length - 1].cssRules.length)