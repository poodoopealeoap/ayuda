var gulp = require('gulp'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    uglify = require('gulp-uglify'),
    coffee = require('gulp-coffee'),
    jade = require('gulp-jade'),
    sass = require('gulp-sass'),
    del = require('del');

gulp.task('coffee', function() {
    return gulp.src('src/**/*.coffee')
        .pipe(concat('ayuda.coffee'))
        .pipe(coffee())
        .pipe(gulp.dest('dist'));
});

gulp.task('min', ['coffee'], function() {
    return gulp.src('dist/ayuda.js')
        .pipe(uglify({mangle:false}))
        .pipe(rename('ayuda.min.js'))
        .pipe(gulp.dest('dist'));
});

gulp.task('sass', function() {
    return gulp.src('src/scss/ayuda.sass')
        .pipe(sass())
        .pipe(gulp.dest('dist'));
});

gulp.task('docs', function() {
    return gulp.src('src/docs/Documentation.jade')
        .pipe(jade())
        .pipe(gulp.dest('./'));
});

gulp.task('watch', ['min','docs','sass'], function() {
    gulp.watch('src/**/*.coffee', ['min']);
    gulp.watch('src/**/*.jade', ['docs']);
    gulp.watch('src/**/*.sass', ['sass']);
});

gulp.task('clean', function() {
    return del(['dist','Documentation.html']);
});

gulp.task('default', ['min','docs','sass']);